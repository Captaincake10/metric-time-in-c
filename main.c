#include <stdio.h>
#include <time.h>
int main() {
  time_t seconds;
  struct tm *timeStruct;
  seconds = time(NULL);
  timeStruct = localtime(&seconds);
  // gets current time
  int RH = timeStruct->tm_hour;
  int RM = timeStruct->tm_min;
  int RS = timeStruct->tm_sec;
  // creates home grown time stamp
  int RTS = (RH * 3600) + (RM * 60) + RS;
  // converts timestamp to metric thought the set factor to make the conversion
  // work
  int MTS = RTS / 0.864;
  // sets metriuc hours and minuets
  int MH = (MTS / 10000);
  int MM = (MTS % 10000) / 100; // int MS  = (MTS % 10000) % 100;
  // printf("%02d:%02d:%02d\r", MH, MM, MS); //with secconds to get secconds
  // uncomment above line
  printf("%02d:%02d\r", MH, MM); // without secconds
  main();
}